(function () {

    /* ---------------------------------- Local Variables ---------------------------------- */
    HomeView.prototype.template = Handlebars.compile($("#home-tpl").html());
    LocationView.prototype.template = Handlebars.compile($("#location-tpl").html());
    SamplingView.prototype.template = Handlebars.compile($("#sampling-tpl").html());
    PhotoView.prototype.template = Handlebars.compile($("#photo-tpl").html());
    DetailsView.prototype.template = Handlebars.compile($("#details-tpl").html());
    SubmitView.prototype.template = Handlebars.compile($("#submit-tpl").html());
    AboutView.prototype.template = Handlebars.compile($("#about-tpl").html());

    // build the data structure in the global space and set some default values
    var wormData = [new Sighting()];
    
    /* ---------------------------------- Local Functions ---------------------------------- */
    
//     var checkForAndroid = function () {
         /* check if this is android and modify select boxes to fix appearance issues. Bug in android causing height and other attributes to be ignored, so turn off webkit stuff */
//          var platform = device.platform;
//          alert(platform);
//          var nua = navigator.userAgent
//          var isAndroid = (nua.indexOf('Mozilla/5.0') > -1 && nua.indexOf('Android ') > -1 && nua.indexOf('AppleWebKit') > -1 && nua.indexOf('Chrome') === -1)
//          if (isAndroid) {
//          	$('select.form-control').addClass('androidselect')
//          }
//     }
    
	router.addRoute('', function() {
		var homeView = new HomeView(wormData);
		$('body').html(homeView.render().$el);
		
		// make background fill the screen on all mobile devices
		if (window.cordova) {
			$div = $('.container');
			$div.removeClass('container');
			$div.addClass('container-fluid');
		}

		var $window = $(window).on('resize', function () {
			homeView.resizehomediv();
		});
		
		$window.resize();
	});
	
	router.addRoute('locationView', function() {
		var locationView = new LocationView(wormData);
		$('body').html(locationView.render().$el);

		// make background fill the screen on all mobile devices
		if (window.cordova) {
			$div = $('.container');
			$div.removeClass('container');
			$div.addClass('container-fluid');
		}

		// after dom built out, use wormData to set inputs and add an event handler to capture data
		locationView.setDefaults();
		
// 		checkForAndroid();
		
	});
	
	router.addRoute('samplingView', function() {
		var samplingView = new SamplingView(wormData);
		$('body').html(samplingView.render().$el);
		
		// make background fill the screen on all mobile devices
		if (window.cordova) {
			$div = $('.container');
			$div.removeClass('container');
			$div.addClass('container-fluid');
		}

		// after dom built out, use wormData to set inputs and add an event handler to capture data
		samplingView.setDefaults();

// 		checkForAndroid();
	});

	router.addRoute('detailsView', function() {
		var detailsView = new DetailsView(wormData);
		$('body').html(detailsView.render().$el);

		// make background fill the screen on all mobile devices
		if (window.cordova) {
			$div = $('.container');
			$div.removeClass('container');
			$div.addClass('container-fluid');
		}

		detailsView.setDefaults();
		
// 		checkForAndroid();
	});

	router.addRoute('photoView', function() {
		var photoView = new PhotoView(wormData);
		$('body').html(photoView.render().$el);
		
		// make background fill the screen on all mobile devices
		if (window.cordova) {
			$div = $('.container');
			$div.removeClass('container');
			$div.addClass('container-fluid');
		}

		photoView.setDefaults();
	});
	
	router.addRoute('submitView', function() {
		var submitView = new SubmitView(wormData);
		$('body').html(submitView.render().$el);

		// make background fill the screen on all mobile devices
		if (window.cordova) {
			$div = $('.container');
			$div.removeClass('container');
			$div.addClass('container-fluid');
		}

		submitView.setupEvents();
		submitView.setDefaults();

// 		checkForAndroid();
		
	});

	router.addRoute('aboutView', function() {
		var aboutView = new AboutView(wormData);
		$('body').html(aboutView.render().$el);
		aboutView.showVersion();
		
		// make background fill the screen on all mobile devices
		if (window.cordova) {
			$div = $('.container');
			$div.removeClass('container');
			$div.addClass('container-fluid');
		}

	});
	
	router.start();
	
    /* --------------------------------- Event Registration -------------------------------- */
    document.addEventListener('deviceready', function () {
    	console.log("Device is Ready");
    	StatusBar.overlaysWebView( false );
		StatusBar.backgroundColorByHexString('#ffffff');
		StatusBar.styleDefault();
    	FastClick.attach(document.body);
    	
	  if (navigator.notification) { // Override default HTML alert with native dialog
		  window.alert = function (message) {
			  navigator.notification.alert(
				  message,    // message
				  null,       // callback
				  "Error",    // title
				  'OK'        // buttonName
			  );
		  };
	  }

	}, false);

}());