var SubmitView = function (wormData) {
  this.config = {
    apikey: 'apikey-goes-here'
  };

	this.initialize = function () {
        // Define a div wrapper for the view (used to attach events)
        this.$el = $('<div class="templatehtml"/>');
        //this.render();
        this.wormData = wormData[wormData.length - 1];
		console.log('SubmitView');
		self = this;
		// make a var for the spinner to be used when submitting
		this.spinner = null;
		this.uploadCount = 0;
    };
    
    this.render = function() {
        this.$el.html(this.template());
        return this;
	};
	
	this.captureData = function () {
		this.wormData.userType = $('#userType').val();
		// this.wormData.age = $('#age').val();
		this.wormData.groupID = $('#groupID').val();
		this.wormData.schoolName = $('#schoolName').val();
	};

	this.setupEvents = function () {
		// modal help handler
		$('#helpModal').on('show.bs.modal', function (event) {
		  var $button = $(event.relatedTarget); // Button that triggered the modal
		  var helpTitle = $button.data('title'); // Extract info from data-* attributes
		  var tmplSelector = $button.data('html');
		  var helpText = $('#modalTemplates').find(tmplSelector).html();
		  var $this = $(this);
		  $this.find('.modal-title').text(helpTitle);
		  $this.find('.modal-body').html(helpText);
		});
		
		// hide the extra buttons if modal has been shown
		$('#helpModal').on('hide.bs.modal', function (event) {
		  var $this = $(this);
		  $this.find('#cancelSighting').addClass('hidden');
// 		  $this.find('#newLocation').addClass('hidden');
// 		  $this.find('#moreWorms').addClass('hidden');
		  $this.find('#okButton').removeClass('hidden');
		});
		
		$('#uploadAll').on('click', function () {
			self.captureData();
			self.prepareToUpload();
		});

		$('#newSighting').on('click', function () {
			self.captureData();
			self.newSighting();
		});
		
		$('#moreWorms').on('click', function () {
			self.captureData();
			self.moreWorms();
		});

		$('#userType').on('change', self.testUserType);
		$('#groupID').on('change', self.testGroupId);
		// $('#age').on('change', self.testAge);
		
		// capture all data on screen if either nav button is tapped
		$('a').slice(0,2).on('click',function (){
			self.captureData();
		});

        // for Upload Sightings modal cancel button if current sighting is incomplete
		$('#cancelSighting').on('click', function(e) {
			e.preventDefault();
			// only attempt to upload if there are more sightings, otherwise just reset
			if (wormData.length <= 1) {
				wormData[0] = new Sighting();
				window.location.href = "#";
			} else {
				//remove the currently unfinished sighting from the array
 				wormData.pop();
				self.wormData = wormData[wormData.length - 1];
// 				self.setDefaults();
				// set the badge on Upload All button with the updated number of completed sightings
				$('.badge').html(wormData.length);
			// console.log('Discard button tapped, length: ' + wormData.length);
				self.prepareToUpload();
			}
        });
        
		$('#uploadCompleteModal').on('hidden.bs.modal', function (e) {
			// make sure there is a valid wormData object if the user dismisses the modal without using the OK button
			if (wormData.length == 0) {
				wormData[0] = new Sighting();
				window.location.href = "#";
			}
		});		
	};
	
	this.setDefaults = function () {
		$('#userType').val(this.wormData.userType);
		// $('#age').val(this.wormData.age);
		$('#groupID').val(this.wormData.groupID);
		$('#schoolName').val(this.wormData.schoolName);

		// highlight required inputs with bad data
		if (this.wormData.submitStarted) {
			if ($('#userType').val().length == 0 )
				$('#userType').parent().addClass('has-error');
			if ($('#groupID').val().length == 0)
				$('#groupID').parent().addClass('has-error');
		}
		
		// put up warning modal if there are still errors on the page
		if ($('div.has-error').length > 0 && self.wormData.submitModalPermitted) {
			var $modal = $('#helpModal');
		  	$modal.find('.modal-title').text("Puutteita löytynyt");
		  	$modal.find('.modal-body').html('Tähdellä merkityissä kentissä on <strong>puutteellisia</strong> tietoja.<br>Puutteellisesti täytetyt kentät on merkitty <span style="color:red">punaisella</span>.');
		  	$modal.modal('show');
		  	// turn off modal error dialog if its been displayed once already
		  	this.wormData.submitModalPermitted = false;
		}
		
		// set the badge on Upload All button with the number of completed sightings
		$('.badge').html(wormData.length);
		
		// jump back directly to samplingView if no worms were found
		if (this.wormData.wormsFound == "Ei") {
			$('#backButton').attr('href', '#samplingView');
		}
		
		this.validateForUploadButton();
	};
	
	this.validateForUploadButton = function () {
		console.log("Validate for upload called");
		var completed = self.validateAll();
		if (completed) {
			$('.fixedSizeButton').removeClass('btn-danger');
			$('.fixedSizeButton').addClass('btn-success');
		} else {
			$('.fixedSizeButton').removeClass('btn-success');
			$('.fixedSizeButton').addClass('btn-danger');
			
			if (this.wormData.submitStarted) {
				// set all "Started" flags to true so fields will highlight if the view hasn't been "started"
				this.wormData.locationStarted = true;
				this.wormData.samplingStarted = true;
				this.wormData.detailsStarted = true;
// 				this.wormData.submitStarted = true;
			}
		}
	};
	
	this.validateAll = function () {
		var completed = true;
		//var currentSighting = wormData[wormData.length - 1];
		// test for required inputs
		if (this.wormData.desc.length == 0) completed = false;
		if (this.wormData.latitude.length == 0) completed = false;
		if (this.wormData.longitude.length == 0) completed = false;
		// if (this.wormData.litter.length == 0) completed = false;
		// if (this.wormData.date.length == 0) completed = false;
		// if (this.wormData.weather.length == 0) completed = false;
		// if (this.wormData.moisture.length == 0) completed = false;
		// if (this.wormData.samplingmethod.length == 0) completed = false;
		// if (this.wormData.standardPlot.length == 0) completed = false;
		if (this.wormData.wormsFound.length == 0) completed = false;
		if (this.wormData.userType.length == 0) completed = false;
		if (this.wormData.groupID.length == 0) completed = false;

		// only test for these required inputs if worms were found
		if (this.wormData.wormsFound == "Kyllä") {
			// if (this.wormData.count.length == 0) completed = false;
			// if (this.wormData.wormType.length == 0) completed = false;
			// if (this.wormData.wormAgeType.length == 0) completed = false;
			// if (this.wormData.wormLength.length == 0) completed = false;
			// if (this.wormData.colour.length == 0) completed = false;
			// if (this.wormData.userType.length == 0) completed = false;
		}
		
		return completed;
	}

	this.newSighting = function () {
		var completed = this.validateAll();
		if (completed) {
			// only keep the user data and in the new sighting
			var newWormData = new Sighting();
			newWormData.userType = self.wormData.userType;
			// newWormData.age = self.wormData.age;
			newWormData.groupID = self.wormData.groupID;
			
			wormData[wormData.length] = newWormData;
			window.location.href = "#locationView";

		} else {
			var $modal = $('#helpModal');
		  	$modal.find('.modal-title').text("Näyteala keskeneräinen");
		  	$modal.find('.modal-body').html('Tähdellä <strong>merkityissä</strong> kentissä on puutteellisia tietoja.<br>Puutteellisesti täytetyt kentät on merkitty <span style="color:red">punaisella</span>.<br>Käytä painiketta <strong>takaisin</strong> mennäksesi edellisiin sivuihin.');
		  	$modal.modal('show');
		  	
		  	// set all "Started" flags to true so fields will highlight if the view hasn't been "started"
			this.wormData.locationStarted = true;
			this.wormData.samplingStarted = true;
			this.wormData.detailsStarted = true;
			this.wormData.submitStarted = true;

		}
	};
	
	this.moreWorms = function () {
		var completed = this.validateAll();
		if (completed) {
			if (self.wormData.wormsFound) {
				// make a copy of the current wormData and remove the details and photo data
				var newWormData = jQuery.extend({},self.wormData);
				newWormData.wormType = "";
				newWormData.count = "";
				newWormData.wormLength = "";
				newWormData.colour = "";
				newWormData.gradient = false;
				newWormData.clitellum = false;
				newWormData.diameterGT2 = false;
				newWormData.tailFlattens = false;
				newWormData.detailsStarted = false;
				newWormData.notes = "";
				newWormData.photo = "";
				newWormData.locationModalPermitted = true;
				newWormData.samplingModalPermitted = true;
				newWormData.detailsModalPermitted = true;
				newWormData.submitModalPermitted = true;
				// add the new wormData object to the worm array
				wormData[wormData.length] = newWormData;
				window.location.href = "#detailsView";
				
			} else {
				var $modal = $('#helpModal');
				$modal.find('.modal-title').text("Error");
				$modal.find('.modal-body').html('You can only start a new <strong>More Worms</strong> sighting if you found worms on your previous sighting.');
				$modal.modal('show');
			}
		
		} else {
			var $modal = $('#helpModal');
		  	$modal.find('.modal-title').text("Näyteala keskeneräinen");
		  	$modal.find('.modal-body').html('Tähdellä <strong>merkityissä</strong> kentissä on puutteellisia tietoja.<br>Puutteellisesti täytetyt kentät on merkitty <span style="color:red">punaisella</span>.<br>Käytä painiketta <strong>takaisin</strong> mennäksesi edellisiin sivuihin.');
		  	$modal.modal('show');
		  	
		  	// set all "Started" flags to true so fields will highlight if the view hasn't been "started"
			this.wormData.locationStarted = true;
			this.wormData.samplingStarted = true;
			this.wormData.detailsStarted = true;
			this.wormData.submitStarted = true;

		}
	}

	this.prepareToUpload = function () {
		var completed = this.validateAll();
		
		// only try to upload if current sighting is validated
		if (completed) {
			// start up an activity indicator
			var opts = {
			  lines: 11, // The number of lines to draw
			  length: 8, // The length of each line
			  width: 5, // The line thickness
			  radius: 10, // The radius of the inner circle 30
			  corners: 1, // Corner roundness (0..1)
			  rotate: 0, // The rotation offset
			  direction: 1, // 1: clockwise, -1: counterclockwise
			  color: '#000', // #rgb or #rrggbb or array of colors
			  speed: 1, // Rounds per second
			  trail: 60, // Afterglow percentage
			  shadow: false, // Whether to render a shadow
			  hwaccel: false, // Whether to use hardware acceleration
			  className: 'spinner', // The CSS class to assign to the spinner
			  zIndex: 2e9, // The z-index (defaults to 2000000000)
			  top: '50%', // Top position relative to parent
			  left: '50%' // Left position relative to parent
			};
			var target = document.getElementById('spinnerDiv');
			if(this.spinner == null) {
			  this.spinner = new Spinner(opts).spin(target);
			} else {
			  this.spinner.spin(target);
			}
		
			self.uploadToServer();
			
		} else {
			var $modal = $('#helpModal');
		  	$modal.find('.modal-title').text("Näyteala keskeneräinen");
		  	$modal.find('.modal-body').html('Tähdellä <strong>merkityissä</strong> kentissä on puutteellisia tietoja.<br>Puutteellisesti täytetyt kentät on merkitty <span style="color:red">punaisella</span>.<br>Käytä painiketta <strong>takaisin</strong> mennäksesi edellisiin sivuihin. Paina <strong>hylkää</strong> poistaaksesi tiedot keskeneräisestä näytealasta ja lähettääksesi tallennetut valmiit näytealat.');
		  	// show the Cancel Sighting button
		  	$modal.find('#cancelSighting').removeClass('hidden');
		  	$modal.modal('show');
		  	
		  	// set all "Started" flags to true so fields will highlight if the view hasn't been "started"
			this.wormData.locationStarted = true;
			this.wormData.samplingStarted = true;
			this.wormData.detailsStarted = true;
			this.wormData.submitStarted = true;
			this.testUserType();
			this.testGroupId();

		}
	};

	this.uploadToServer = function () {
		// test mobile for data connection before uploading
		if (window.cordova) {
			var networkState = navigator.connection.type;
// 			alert('network state: ' + networkState);
			if (networkState == 'none') {
				this.spinner.stop();
				var $modal = $('#helpModal');
				$modal.find('.modal-title').text("Ei datayhteyttä");
				$modal.find('.modal-body').html('Et ole yhteydessä internetiin. Varmista, että internet-yhteytesi on päällä, tai yritä lähettää havaintosi myöhemmin uudelleen, kun olet yhteydessä internetiin.');
				$modal.modal('show');
				return;
			}
		}

		if ( wormData.length > 0 ) {
			var target = document.getElementById('spinnerDiv');
			this.spinner.spin(target);

			console.log("INFO: Worm data length before uploading: " + wormData.length);
			// capture and remove the first wormData element from the array
			var currentWormData = wormData.shift();
			console.log("INFO: Worm data length after uploading: " + wormData.length);
			// set the badge on Upload All button with the number of completed sightings left
			$('.badge').html(wormData.length);

			var dataForUpload = {
				'photo': "",
				'habitat': currentWormData.desc,
				'latitude': currentWormData.latitude,
				'longitude': currentWormData.longitude,
				'overrideGPS': currentWormData.gpsoverride,
				'paths': currentWormData.impactpaths,
				'roads': currentWormData.impactroads,
				'buildings': currentWormData.impactbuildings,
				'fishing': currentWormData.impactfishing,
				'grazing': currentWormData.impactgrazing,
				'crops': currentWormData.impactcrops,
				'lakeOrStream': currentWormData.impactterrain,
				'litterLayer': currentWormData.litter,
				'sampledAt': currentWormData.date,
				'weather': currentWormData.weather,
				'groundMoisture': currentWormData.moisture,
				'samplingMethod': currentWormData.samplingMethod,
				'standardPlot': currentWormData.standardPlot,
				'foundWorms': currentWormData.wormsFound,
				'adultLitterDwellingHandExtraction': currentWormData.adultLitterDwellingHandExtraction,
				'adultLitterDwellingMustardExtraction': currentWormData.adultLitterDwellingMustardExtraction,
				'juvinelleLitterDwellingHandExtraction': currentWormData.juvinelleLitterDwellingHandExtraction,
				'juvinelleLitterDwellingMustardExtraction': currentWormData.juvinelleLitterDwellingMustardExtraction,
				'adultSoilDwellingHandExtraction': currentWormData.adultSoilDwellingHandExtraction,
				'adultSoilDwellingMustardExtraction': currentWormData.adultSoilDwellingMustardExtraction,
				'juvinelleSoilDwellingHandExtraction': currentWormData.juvinelleSoilDwellingHandExtraction,
				'juvinelleSoilDwellingMustardExtraction': currentWormData.juvinelleSoilDwellingMustardExtraction,
				'adultDeepBurrowingHandExtraction': currentWormData.adultDeepBurrowingHandExtraction,
				'adultDeepBurrowingMustardExtraction': currentWormData.adultDeepBurrowingMustardExtraction,
				'juvinelleDeepBurrowingHandExtraction': currentWormData.juvinelleDeepBurrowingHandExtraction,
				'juvinelleDeepBurrowingMustardExtraction': currentWormData.juvinelleDeepBurrowingMustardExtraction,
				'scientist': currentWormData.userType,
				'groupId': currentWormData.groupID,
				'schoolName': currentWormData.schoolName,
				'appVersion': currentWormData.appVersion
		  };
		  
		  // these are integers, so will generate an error if they are sent as null
		  if (!currentWormData.middenCount.length > 0) {
		  	currentWormData.middenCount = 0;
		  	currentWormData.count = 0;
		  }
		  if (currentWormData.middenCount.length > 0) dataForUpload.middenCount = currentWormData.middenCount;
		  // if (currentWormData.age.length > 0) dataForUpload.scientistAge = currentWormData.age;
		  
		  // convert these to booleans - any non-true string sent is evaluated as false so condition for 'No" not needed
		  if (currentWormData.wormsFound == "Kyllä") dataForUpload.foundWorms = 'true';
		  if (currentWormData.standardPlot == "Kyllä") dataForUpload.standardPlot = 'true';

		  // only send the details data if worms were found
		  if (currentWormData.wormsFound == "Kyllä") {
			dataForUpload['sighting[wormCount]'] = currentWormData.middenCount;
			dataForUpload['sighting[classification]'] = currentWormData.wormType;
			dataForUpload['sighting[length]'] = currentWormData.wormLength;
			dataForUpload['sighting[colour]'] = currentWormData.colour;
			dataForUpload['sighting[colourGradient]'] = currentWormData.gradient;
			dataForUpload['sighting[clitellumAbsent]'] = currentWormData.clitellum;
			dataForUpload['sighting[diamGT2mm]'] = currentWormData.diameterGT2;
			dataForUpload['sighting[tailFlattens]'] = currentWormData.tailFlattens;
			dataForUpload['sighting[notes]'] = currentWormData.notes;
		  }

		} else {
			// no more worm data
			// console.log('no more worm data');
			this.spinner.stop();
			var $modal = $('#uploadCompleteModal');
			$modal.find('.modal-title').text("Lataus valmis");
			var message = '1 näyteala';
			if (self.uploadCount > 1) {
				message = self.uploadCount + ' näytealaa';
			}
			$modal.find('.modal-body').html(message + ' on onnistuneesti ladattu. Kiitos osallistumisestasi.');
		  	$modal.modal('show');
		  	
			return;
		}

		// use the file-transfer plugin if there is a photo
		if (currentWormData.photo.length > 0) {
			self.uploadWithPhoto(dataForUpload, currentWormData, currentWormData.photo.length);
			
		} else {
			self.uploadNoPhoto(dataForUpload, currentWormData);
// 			setTimeout(function(){ self.uploadNoPhoto(dataForUpload, currentWormData) }, 3000);
		}
	}
	
	this.uploadWithPhoto = function (dataForUpload, currentWormData, photoCount) {
		var win = function (r) {
// 			console.log("Code = " + r.responseCode);
// 			console.log("Response = " + r.response);
// 			console.log("Sent = " + r.bytesSent);
			self.uploadCount ++;

		}

		var finalWin = function (r) {
			win();

			self.uploadToServer(); // try to upload another one
		}

		var fail = function (error) {
// 			console.log("An error has occurred: Code = " + error.code);
// 			console.log("An error has occurred: Error = " + error.http_status);
// 			console.log("upload error source " + error.source);
// 			console.log("upload error target " + error.target);
			// something bad happened so send the failed sighting the uploadFailed to be saved
			self.uploadFailed(currentWormData, error.http_status);
		}

		for (i = 0; i < photoCount; i++) {

			var fileURL = currentWormData.photo[i];
			// remove the photo property now so it doesn't get included in the data upload
			delete dataForUpload.photo;

			var options = new FileUploadOptions();
			options.fileKey = "sighting[image]";
			options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
			options.mimeType = "image/jpeg";
			options.headers = {"X-API-Key": self.config.apikey};
			options.params = dataForUpload;
	// 		options.chunkedMode = false;

			var ft = new FileTransfer();
			if (i == (photoCount -1)) {
				ft.upload(fileURL, encodeURI("http://h244.it.helsinki.fi:3080/sample"), finalWin, fail, options);
			} else {
				ft.upload(fileURL, encodeURI("http://h244.it.helsinki.fi:3080/sample"), win, fail, options);
			}

		}
	}
	
	this.uploadNoPhoto = function(dataForUpload, currentWormData) {
		// console.log('Uploading without a photo');
		// remove the photo property so it doesn't get included in the data upload
		delete dataForUpload.photo;
		
		$.ajax({
			url: 'http://h244.it.helsinki.fi:3080/sample',
			data: dataForUpload,
			dataType: 'json',
			
			success: function( result, textStatus, jqXHR) {
				// console.log('success: ' + jqXHR.status );
				// console.log(result);
				self.uploadCount ++;
				self.uploadToServer(); // try to upload another one
			},
			
			error: function( jqXHR, textStatus, textErrorMsg) {
				alert('An error has occurred: ' + jqXHR.status);
//				alert('Response: ' + jqXHR.responseText);
				// something bad happened so send the failed sighting the uploadFailed to be saved
				alert('error: ' + jqXHR.responseText);				self.uploadFailed(currentWormData, jqXHR.status);
			},
			
			headers: {"X-API-Key": self.config.apikey},
			type: 'POST'
		});	
	}
	
	this.uploadFailed = function(currentWormData, errorCode) {
		// stop the activity indicator
		this.spinner.stop();
	
		// set up the modal dialog error message based on the returned error code
		var $modal = $('#helpModal');
		
		if (errorCode == 400) {
			$modal = $('#uploadCompleteModal');
			$modal.find('.modal-title').text("Upload Problem");
			$modal.find('.modal-body').html('<p>There was a problem with some of the data submitted.  We have saved your data, however your sightings may not be available when exploring data on the Worm Tracker website.</p><p><small>Error code: ' + errorCode + '</small></p>');
			
			// no need to save the failed sighting as the server will save the invalid data
			
		} else {
			$modal.find('.modal-title').text("Virhe tietojen lataamisessa");
			$modal.find('.modal-body').html('<p>Matoseurannan palvelin ei tällä hetkellä ole saavutettavissa. Ole hyvä ja lataa näytealasi tiedot myöhemmin uudelleen.</p><p><small>Virhekoodi: ' + errorCode + '</small></p>');
			
			// put the failed sighting back into the sightings array
			wormData.unshift(currentWormData);
		}
		
		// update the badge with the current number of sightings
		$('.badge').html(wormData.length);
		$modal.modal('show');
		// console.log('num of sightings: '+ wormData.length);
	}
		
	this.testUserType = function () {
		self.wormData.submitStarted = true;
		var $userType = $('#userType');
		if ($userType.val().length == 0) {
			$userType.parent().addClass('has-error');
		} else {
			$userType.parent().removeClass('has-error');
		}
		self.captureData();
		self.validateForUploadButton();
	};
	
	// convert group id to lower case and add the red notification
	this.testGroupId = function () {
		self.wormData.submitStarted = true;
		var id = $('#groupID').val();
		if (id.length == 0) {
			$('#groupID').parent().addClass('has-error');
		} else {
			$('#groupID').parent().removeClass('has-error');
		}
		$('#groupID').val(id.toLowerCase());
		self.validateForUploadButton();
	}

	/*
	this.testAge = function () {
		self.wormData.submitStarted = true;
		var $age = $('#age');
		var parsedAge = parseInt($age.val(),10);
		if (parsedAge >= 1 && parsedAge <= 123) {
			$age.parent().removeClass('has-error');
			self.wormData.age = parsedAge;
			$age.val(parsedAge);
		} else {
			$age.parent().addClass('has-error');
			$age.val("");
			$age.parent().popover('show');
		}
	};
	*/
	
    this.initialize();

}