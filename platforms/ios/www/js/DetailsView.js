var DetailsView = function (wormData) {

	this.initialize = function () {
        // Define a div wrapper for the view (used to attach events)
        this.$el = $('<div class="templatehtml"/>');
        //this.render();
        this.wormData = wormData[wormData.length - 1];
        // console.log('detailsview: ');
		self = this;
    };
    
    this.render = function() {
        this.$el.html(this.template());
        return this;
	};
	
	this.captureData = function () {
		// this.wormData.count = $('#count').val();
		this.wormData.wormType = $('#wormType').val();
		this.wormData.wormAgeType = $('#wormAgeType').val();
		this.wormData.samplingmethod = $('#samplingmethod').val();
		// set defaults, #TODO: to remove
		this.wormData.adultLitterDwellingHandExtraction = 0;
		this.wormData.adultLitterDwellingMustardExtraction = 0;
		this.wormData.juvinelleLitterDwellingHandExtraction = 0;
		this.wormData.juvinelleLitterDwellingMustardExtraction = 0;
		this.wormData.adultSoilDwellingHandExtraction = 0;
		this.wormData.adultSoilDwellingMustardExtraction = 0;
		this.wormData.juvinelleSoilDwellingHandExtraction = 0;
		this.wormData.juvinelleSoilDwellingMustardExtraction = 0;
		this.wormData.adultDeepBurrowingHandExtraction = 0;
		this.wormData.adultDeepBurrowingMustardExtraction = 0;
		this.wormData.juvinelleDeepBurrowingHandExtraction = 0;
		this.wormData.juvinelleDeepBurrowingMustardExtraction = 0;
		// capture the extarction values
		if ($('#adultLitterDwellingHandExtraction').val().length > 0) this.wormData.adultLitterDwellingHandExtraction = $('#adultLitterDwellingHandExtraction').val();
		if ($('#adultLitterDwellingMustardExtraction').val().length > 0) this.wormData.adultLitterDwellingMustardExtraction = $('#adultLitterDwellingMustardExtraction').val();
		if ($('#juvinelleLitterDwellingHandExtraction').val().length >0) this.wormData.juvinelleLitterDwellingHandExtraction = $('#juvinelleLitterDwellingHandExtraction').val();
		if ($('#juvinelleLitterDwellingMustardExtraction').val().length > 0) this.wormData.juvinelleLitterDwellingMustardExtraction = $('#juvinelleLitterDwellingMustardExtraction').val();
		if ($('#adultSoilDwellingHandExtraction').val().length > 0) this.wormData.adultSoilDwellingHandExtraction = $('#adultSoilDwellingHandExtraction').val();
		if ($('#adultSoilDwellingMustardExtraction').val().length >0) this.wormData.adultSoilDwellingMustardExtraction = $('#adultSoilDwellingMustardExtraction').val();
		if ($('#juvinelleSoilDwellingHandExtraction').val().length >0 ) this.wormData.juvinelleSoilDwellingHandExtraction = $('#juvinelleSoilDwellingHandExtraction').val();
		if ($('#juvinelleSoilDwellingMustardExtraction').val().length >0) this.wormData.juvinelleSoilDwellingMustardExtraction = $('#juvinelleSoilDwellingMustardExtraction').val();
		if ($('#adultDeepBurrowingHandExtraction').val().length >0) this.wormData.adultDeepBurrowingHandExtraction = $('#adultDeepBurrowingHandExtraction').val();
		if ($('#adultDeepBurrowingMustardExtraction').val().length >0) this.wormData.adultDeepBurrowingMustardExtraction = $('#adultDeepBurrowingMustardExtraction').val();
		if ($('#juvinelleDeepBurrowingHandExtraction').val().length >0) this.wormData.juvinelleDeepBurrowingHandExtraction = $('#juvinelleDeepBurrowingHandExtraction').val();
		if ($('#juvinelleDeepBurrowingMustardExtraction').val().length >0) this.wormData.juvinelleDeepBurrowingMustardExtraction = $('#juvinelleDeepBurrowingMustardExtraction').val();
	}
	
	this.setDefaults = function () {
		this.setupEvents();
		$('#wormAgeType').val(this.wormData.wormAgeType);
		$('#wormType').val(this.wormData.wormType);
		$('#samplingmethod').val(this.wormData.samplingmethod);
		$('#adultLitterDwellingHandExtraction').val(this.wormData.adultLitterDwellingHandExtraction);
		$('#adultLitterDwellingMustardExtraction').val(this.wormData.adultLitterDwellingMustardExtraction);
		$('#juvinelleLitterDwellingHandExtraction').val(this.wormData.juvinelleLitterDwellingHandExtraction);
		$('#juvinelleLitterDwellingMustardExtraction').val(this.wormData.juvinelleLitterDwellingMustardExtraction);
		$('#adultSoilDwellingHandExtraction').val(this.wormData.adultSoilDwellingHandExtraction);
		$('#adultSoilDwellingMustardExtraction').val(this.wormData.adultSoilDwellingMustardExtraction);
		$('#juvinelleSoilDwellingHandExtraction').val(this.wormData.juvinelleSoilDwellingHandExtraction);
		$('#juvinelleSoilDwellingMustardExtraction').val(this.wormData.juvinelleSoilDwellingMustardExtraction);
		$('#adultDeepBurrowingHandExtraction').val(this.wormData.adultDeepBurrowingHandExtraction);
		$('#adultDeepBurrowingMustardExtraction').val(this.wormData.adultDeepBurrowingMustardExtraction);
		$('#juvinelleDeepBurrowingHandExtraction').val(this.wormData.juvinelleDeepBurrowingHandExtraction);
		$('#juvinelleDeepBurrowingMustardExtraction').val(this.wormData.juvinelleDeepBurrowingMustardExtraction);
		
		// highlight required inputs with bad data
		if (this.wormData.detailsStarted) {
			// this.testClitellum();

			// if ($('#count').val().length == 0) {
			// 	$('#count').parent().addClass('has-error');
			// };
			if ($('#wormType').val().length == 0) {
				$('#wormType').parent().addClass('has-error');
			};
			if ($('#wormAgeType').val().length == 0) {
				$('#wormAgeType').parent().addClass('has-error');
			};
			if ($('#samplingmethod').val().length == 0) {
				$('#samplingmethod').parent().addClass('has-error');
			};
			// if ($('#colour').val().length == 0) {
			// 	$('#colour').parent().addClass('has-error');
			// };
		}
		
		// put up warning modal if there are still errors on the page
		if ($('div.has-error').length > 0 && self.wormData.detailsModalPermitted) {
			var $modal = $('#helpModal');
		  	$modal.find('.modal-title').text("Näyteala keskeneräinen");
		  	$modal.find('.modal-body').html('Tähdellä <strong>merkityissä</strong> kentissä on puutteellisia tietoja.<br>Puutteellisesti täytetyt kentät on merkitty <span style="color:red">punaisella</span>.');
		  	$modal.modal('show');
		  	// turn off modal error dialog if its been displayed once already
			self.wormData.detailsModalPermitted = false;
		}

		if (self.wormData.showDetailsAboutModal) {
			var $modal = $('#helpModal');
		  	$modal.find('.modal-title').text("Yksityiskohdat");
		  	$modal.find('.modal-body').html('<p>Tälle sivulle tallennetaan tiedot löytämistäsi lierotyypeistä. Tallenna erikseen tiedot sinappiveden avulla löydetyistä lieroista ja käsin löydetyistä lieroista. Ennen kuin tallennat tiedot, tunnista mihin kolmeen lierotyyppiin lierot kuuluvat. Erottele ensin aikuiset ja nuoret yksilöt, jonka jälkeen tunnista, ovatko ne karikkeessa eläviä lieroja, vaakakaivajia vai syväkaivajia.</p>Jos et ole varma lierotyypistä, klikkaa kohdasta “Lierotyyppi” saadaksesi lisätietoa.');
		  	$modal.modal('show');
		  	self.wormData.showDetailsAboutModal = false;
		}

	}

	this.setupEvents = function () {
		// modal help handler
		$('#helpModal').on('show.bs.modal', function (event) {
		  var $button = $(event.relatedTarget); // Button that triggered the modal
		  var helpTitle = $button.data('title'); // Extract info from data-* attributes
		  var tmplSelector = $button.data('html');
		  var helpText = $('#modalTemplates').find(tmplSelector).html();
		  var $this = $(this);
		  $this.find('.modal-title').text(helpTitle);
		  $this.find('.modal-body').html(helpText);
		});
		
		$('#wormAgeType').on('change', self.testWormType);
		$('#wormType').on('change', self.testWormType);
		$('#samplingmethod').on('change', self.testsamplingmethod);
		// $('#colour').on('change', self.testColour);
		// $('#clitellum').on('click', self.testClitellum);
		
		// capture all data on screen if either nav button is tapped
		$('a').slice(0,2).on('click',function (){
			self.captureData();
		});



	}

	/*
	this.testCount = function () {
		self.wormData.detailsStarted = true;
		var $count = $('#count');
		var val = $count.val();
		var countParsed = parseInt(val,10);
		if (countParsed > 0 ) {
			$count.parent().removeClass('has-error');
			$count.val(countParsed);
		} else {
			$count.parent().addClass('has-error');
			$count.val("");
		}
	}
	*/

	this.testWormType = function () {
		self.wormData.detailsStarted = true;
		var $wormType = $('#wormType');
		if ($wormType.val().length == 0) {
			$wormType.parent().addClass('has-error');
		} else {
			$wormType.parent().removeClass('has-error');
		}
	}
	
	this.testsamplingmethod = function () {
		self.wormData.detailsStarted = true;
		var $wormSamplingMethod = $('#samplingmethod');
		if ($wormSamplingMethod.val().length == 0) {
			$wormSamplingMethod.parent().addClass('has-error');
		} else {
			$wormSamplingMethod.parent().removeClass('has-error');
		}
	}

	/*
	this.testColour = function () {
		self.wormData.detailsStarted = true;
		var $colour = $('#colour');
		if ($colour.val().length == 0) {
			$colour.parent().addClass('has-error');
		} else {
			$colour.parent().removeClass('has-error');
		}
	}
	
	this.testClitellum = function () {
		self.wormData.detailsStarted = true;
		if ($('#clitellum').prop('checked')) {
			$('#diameterGT2').prop('disabled',false);
			$('#diameterGT2').closest("div").removeClass('disabled');
			$('#tailFlattens').prop('disabled',false);
			$('#tailFlattens').closest("div").removeClass('disabled');
			
		} else {
			$('#diameterGT2').prop('disabled',true);
			$('#diameterGT2').prop('checked',false);
			$('#diameterGT2').closest("div").addClass('disabled');
			$('#tailFlattens').prop('disabled',true);
			$('#tailFlattens').prop('checked',false);
			$('#tailFlattens').closest("div").addClass('disabled');
		}
	}
	*/

    this.initialize();

}