var LocationView = function (wormData) {

	this.initialize = function () {
        // Define a div wrapper for the view (used to attach events)
        this.$el = $('<div class="templatehtml"/>');
        //this.render();
        this.wormData = wormData[wormData.length - 1];
//        console.log('LocationView');
		self = this;
    };
    
    this.render = function() {
        this.$el.html(this.template());
        return this;
	};
	
	this.captureData = function () {
		// check to make sure lat/long data is valid before capturing it
		// if GPS is not authorized by user then fields will contain 'Waiting...'
		
		this.testLatitude();
		this.testLongitude();
		
		this.wormData.desc = $('#description').val();
		this.wormData.latitude = $('#latitude').val();
		this.wormData.longitude = $('#longitude').val();
		// this.wormData.litter = $('#litter').val();
		this.wormData.impactpaths = $('#impact-paths').prop('checked');
		this.wormData.impactfishing = $('#impact-fishing').prop('checked');
		this.wormData.impactroads = $('#impact-roads').prop('checked');
		this.wormData.impactgrazing = $('#impact-grazing').prop('checked');
		this.wormData.impactbuildings = $('#impact-buildings').prop('checked');
		this.wormData.impactcrops = $('#impact-crops').prop('checked');
		this.wormData.impactterrain = $('#impact-terrain').prop('checked');
	}
	
	this.setDefaults = function () {
		this.setupEvents();
		// required inputs
		var $desc = $('#description');
		var $lat = $('#latitude');
		var $long = $('#longitude');
		// var $litter = $('#litter');
		$desc.val(this.wormData.desc);
		$lat.val(this.wormData.latitude);
		$long.val(this.wormData.longitude);
		// $litter.val(this.wormData.litter);
		
		// optional inputs
		$('#impact-paths').prop('checked', this.wormData.impactpaths);
		$('#impact-fishing').prop('checked', this.wormData.impactfishing);
		$('#impact-roads').prop('checked', this.wormData.impactroads);
		$('#impact-grazing').prop('checked', this.wormData.impactgrazing);
		$('#impact-buildings').prop('checked', this.wormData.impactbuildings);
		$('#impact-crops').prop('checked', this.wormData.impactcrops);
		$('#impact-terrain').prop('checked', this.wormData.impactterrain);
		$('#gpsoverride').prop('checked', this.wormData.gpsoverride);
		
		// only attempt to look up coordinates gpsoverride is turned off
		if ($('#gpsoverride').prop('checked') == false) {
			this.addLocation();
		} else {
			// otherwise reset the fields to allow data entry
			$('#latitude').prop('readonly', false);
			$('#longitude').prop('readonly', false);
			$('.coordinate input').removeClass('coordinate-text')
		}
		
		// if lat and long fields have already been made writeable, then check the gps override checkbox
// 		if ($('#latitude').prop('readonly') == false) {
// 			$('#gpsoverride').prop('checked', true);
// 		}

		// highlight required inputs with bad data
		if (this.wormData.locationStarted) {
			if ($desc.val().length == 0) {
				$desc.parent().addClass('has-error');
			};
			if ($lat.val().length == 0) {
				$lat.parent().addClass('has-error');
			};
			if ($long.val().length == 0) {
				$long.parent().addClass('has-error');
			};
			// if ($litter.val().length == 0) {
			// 	$litter.parent().addClass('has-error');
			// };
		}
		
		// put up warning modal if there are still errors on the page
		if ($('div.has-error').length > 0 && this.wormData.locationModalPermitted) {
			var $modal = $('#helpModal');
		  	$modal.find('.modal-title').text("Näyteala keskeneräinen");
		  	$modal.find('.modal-body').html('Tähdellä <strong>merkityissä</strong> kentissä on puutteellisia tietoja.<br>Puutteellisesti täytetyt kentät on merkitty <span style="color:red">punaisella</span>.');
		  	$modal.modal('show');
		  	// turn off modal error dialog if its been displayed once already
		  	this.wormData.locationModalPermitted = false;
		}
	}
	
	this.setupEvents = function () {
		// modal help handler
		$('#helpModal').on('show.bs.modal', function (event) {
		  var $button = $(event.relatedTarget); // Button that triggered the modal
		  var helpTitle = $button.data('title'); // Extract info from data-* attributes
		  var tmplSelector = $button.data('html');
		  //console.log(tmplSelector);
		  var helpText = $('#modalTemplates').find(tmplSelector).html();
		  //var helpText = $(tmplSelector).html(); 
		  //console.log(helpText);
		  var $this = $(this);
		  $this.find('.modal-title').text(helpTitle);
		  $this.find('.modal-body').html(helpText);
		});

		$('#description').on('change', self.testDescription);
		$('#latitude').on('change', self.testLatitude);
		$('#longitude').on('change', self.testLongitude);
		// $('#litter').on('change', self.testLitter);
		$('#gpsoverride').on('click', self.gpsoverride);
		$('.checkbox input').on('click', self.checkboxSet);
		
		// capture all data on screen if either nav button is tapped
		$('a').slice(0,2).on('click', function () {
			self.captureData();
		});
	}
	
	this.addLocation = function () {
		// console.log('addLocation');
		$('#latitude').val('Waiting...');
		$('#longitude').val('Waiting...');
  		navigator.geolocation.getCurrentPosition(
			function(position) {
				//alert('Latitude: ' + position.coords.latitude + '\n' +'Longitude: ' + position.coords.longitude);				
// 				$('#latitude').prop('readonly', true);
// 				$('#longitude').prop('readonly', true);
          		$('#latitude').val(position.coords.latitude.toFixed(5));
				$('#longitude').val(position.coords.longitude.toFixed(5));
				$('#gpsoverride').prop('checked', false);
// 				$('.coordinate input').addClass('coordinate-text')
				self.wormData.gpsoverride = false;
			},
      		function(error) {
          		//alert('Code: ' + error.code +'\n' + 'Message: ' + error.message);
				// only show gps error before any data is entered
				//if (wormData.locationStarted === false) {
				var $modal = $('#helpModal');
				$modal.find('.modal-title').text("GPS virhe");
				$modal.find('.modal-body').html('<p>Sijaintiasi ei löydy.</p><p>Voit kirjoittaa sijaintisi koordinaatit.</p>');
				$modal.modal('show');
				//}
				
				$('#latitude').prop('readonly', false);
				$('#longitude').prop('readonly', false);
				$('#gpsoverride').prop('checked', true);
				$('.coordinate input').removeClass('coordinate-text')
				$('#latitude').val('');
				$('#longitude').val('');
				self.wormData.gpsoverride = true;
     		},
      		// maxage set to 30 seconds to prevent multiple lookups if returning to locationview quickly
      		// enabled enableHighAccuracy as Samsung doesn't seem to work consistantly with it false
      		{enableHighAccuracy: false,timeout: 15000, maximumAge: 30000});	
    }
	
	this.gpsoverride = function () {
		if ($('#gpsoverride').prop('checked')) {
			$('#latitude').prop('readonly', false);
			$('#longitude').prop('readonly', false);
			$('#latitude').val('');
			$('#longitude').val('');
			$('.coordinate input').removeClass('coordinate-text')
			self.wormData.gpsoverride = true;
		} else {
			$('#latitude').prop('readonly', true);
			$('#longitude').prop('readonly', true);
			$('.coordinate input').addClass('coordinate-text')
			self.wormData.gpsoverride = false;
			self.addLocation();
		}
	}
	
	this.checkboxSet = function () {
		// checkboxes are optional, but indicate that data entry has started and any existing popovers should be destroyed
		self.wormData.locationStarted = true;
		$('[data-toggle="popover"]').popover('destroy');
	}

	/*
	this.testLitter = function () {
		self.wormData.locationStarted = true;
		$('[data-toggle="popover"]').popover('destroy');
		var $litter = $('#litter');
		var litterParsed = parseInt($litter.val(),10);
		if (litterParsed >= 0 && litterParsed <= 15) {
			$litter.parent().removeClass('has-error');
			$litter.val(litterParsed);
		} else {
			$litter.parent().addClass('has-error');
			$litter.val("");
			$litter.parent().popover('show');
		}
	}
	*/
	
	this.testDescription = function () {
		self.wormData.locationStarted = true;
		$('[data-toggle="popover"]').popover('destroy');
		var $desc = $('#description');
		if ($desc.val().length == 0) {
			$desc.parent().addClass('has-error');
		} else {
			$desc.parent().removeClass('has-error');
		} 
	}

	this.testLatitude = function () {
// 		self.wormData.locationStarted = true;
		$('[data-toggle="popover"]').popover('destroy');
		var $lat = $('#latitude');
		var latParsed = parseFloat($lat.val());
		if (latParsed >= -90 && latParsed <= 90) {
			$lat.parent().removeClass('has-error');
			$lat.val(latParsed);
		} else {
			$lat.parent().addClass('has-error');
			$lat.val("");
			$lat.popover('show');
		}
	}

	this.testLongitude = function () {
// 		self.wormData.locationStarted = true;
		$('[data-toggle="popover"]').popover('destroy');
		var $long = $('#longitude');
		var longParsed = parseFloat($long.val());
		if (longParsed >= -180 && longParsed <= 180) {
			$long.parent().removeClass('has-error');
			$long.val(longParsed);
		} else {
			$long.parent().addClass('has-error');
			$long.val("");
			$long.popover('show');
		}
	}


    this.initialize();

}