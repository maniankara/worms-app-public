var SamplingView = function (wormData) {

	this.initialize = function () {
        // Define a div wrapper for the view (used to attach events)
        this.$el = $('<div class="templatehtml"/>');
        this.wormData = wormData[wormData.length - 1];
        // console.log('SamplingView');
		self = this;
    };
    
    this.render = function() {
        this.$el.html(this.template());
        return this;
	};

	this.captureData = function () {
		// this.wormData.weather = $('#weather').val();
		this.wormData.moisture = $('#moisture').val();
		// this.wormData.samplingMethod = $('#samplingmethod').val();
		this.wormData.middenCount = $('#middenCount').val();
		// this.wormData.standardPlot = $('#standardPlot').val();
		this.wormData.wormsFound = $('#wormsFound').val();
		this.wormData.date = $('#date').val();
		
		// jump directly to submitView if no worms were found
		if (this.wormData.wormsFound == "No") {
			$('a.wormsFoundNext').attr('href', '#submitView');
		}
	}
	
	this.setDefaults = function () {
		this.setupEvents();
		// var $weather = $('#weather');
		var $moisture = $('#moisture');
		// var $samplingmethod = $('#samplingmethod');
		var $date = $('#date');
		var $middenCount = $('#middenCount');
		// var $standardPlot = $('#standardPlot');
		var $wormsFound = $('#wormsFound');
		
		//this.wormData.date = new Date();
		//$('#date').val(new Date().toISOString().substring(0, 10));
		$date.val(this.wormData.date);
		// $weather.val(this.wormData.weather);
		$moisture.val(this.wormData.moisture);
		// $samplingmethod.val(this.wormData.samplingMethod);
		// optional inputs
		$middenCount.val(this.wormData.middenCount);
		// $standardPlot.val(this.wormData.standardPlot);
		$wormsFound.val(this.wormData.wormsFound);
// 		$('#standardPlot').prop('checked', this.wormData.standardPlot);
// 		$('#wormsFound').prop('checked', this.wormData.wormsFound);

		$('#date').val(new Date().toISOString().substring(0, 10));
		// hide date field if running on a device
		if (window.cordova) {
			$('#dateField').addClass('hidden');
		}

		// highlight required inputs with bad data
		if (this.wormData.samplingStarted) {
			if ($date.val().length == 0) {
				$date.parent().addClass('has-error');
			};
			// if ($weather.val().length == 0) {
			// 	$weather.parent().addClass('has-error');
			// };
			if ($moisture.val().length == 0) {
				$moisture.parent().addClass('has-error');
			};
			// if ($samplingmethod.val().length == 0) {
			// 	$samplingmethod.parent().addClass('has-error');
			// };
			// if ($standardPlot.val().length == 0) {
			// 	$standardPlot.parent().addClass('has-error');
			// };
			if ($wormsFound.val().length == 0) {
				$wormsFound.parent().addClass('has-error');
			};
		}
		
		// put up warning modal if there are still errors on the page
		if ($('div.has-error').length > 0 && this.wormData.samplingModalPermitted) {
			var $modal = $('#helpModal');
		  	$modal.find('.modal-title').text("Näyteala keskeneräinen");
		  	$modal.find('.modal-body').html('Tähdellä <strong>merkityissä</strong> kentissä on puutteellisia tietoja.<br>Puutteellisesti täytetyt kentät on merkitty <span style="color:red">punaisella</span>.');
		  	$modal.modal('show');
		  	// turn off modal error dialog if its been displayed once already
		  	this.wormData.samplingModalPermitted = false;
		}
		
	}
	
	this.setupEvents = function () {
		// modal help handler
		$('#helpModal').on('show.bs.modal', function (event) {
		  var $button = $(event.relatedTarget); // Button that triggered the modal
		  var helpTitle = $button.data('title'); // Extract info from data-* attributes
		  var tmplSelector = $button.data('html');
		  var helpText = $('#modalTemplates').find(tmplSelector).html();
		  var $this = $(this);
		  $this.find('.modal-title').text(helpTitle);
		  $this.find('.modal-body').html(helpText);
		});
		
		$('#date').on('change', self.testDate);
		// $('#weather').on('change', self.testWeather);
 		$('#moisture').on('change', self.testMoisture);
 		// $('#samplingmethod').on('change', self.testSamplingMethod);
 		$('#middenCount').on('change', self.testMiddenCount);
 		// $('#standardPlot').on('change', self.testStandardPlot);
 		$('#wormsFound').on('change', self.testWormsFound);
// 		$('.checkbox input').on('click', self.checkboxSet);
		
		// capture all data on screen if either nav button is tapped
		$('a').slice(0,2).on('click',function (){
			self.captureData();
		});
		
	}
	
	this.testDate = function () {
		self.wormData.samplingStarted = true;
		var $date = $('#date');
		
		// if no data or date format is invalid
		if ($date.val().length > 0 && self.isDate($date.val())) {
			$date.parent().removeClass('has-error');
		} else {
			$date.parent().addClass('has-error');
			$date.val("");
		}
	}

	/*
	this.testWeather = function () {
		self.wormData.samplingStarted = true;
		var $weather = $('#weather');
		if ($weather.val().length == 0) {
			$weather.parent().addClass('has-error');
			$weather.val("");
		} else {
			$weather.parent().removeClass('has-error');
		}
	}
	*/
	
	this.testMoisture = function () {
		self.wormData.samplingStarted = true;
		var $moisture = $('#moisture');
		if ($moisture.val().length == 0) {
			$moisture.parent().addClass('has-error');
		} else {
			$moisture.parent().removeClass('has-error');
		}
	}

	/*
	this.testSamplingMethod = function () {
		self.wormData.samplingStarted = true;
		var $samplingmethod = $('#samplingmethod');
		if ($samplingmethod.val().length == 0) {
			$samplingmethod.parent().addClass('has-error');
		} else {
			$samplingmethod.parent().removeClass('has-error');
		}
	}
	*/

	/*
	this.testStandardPlot = function () {
		self.wormData.samplingStarted = true;
		var $standardPlot = $('#standardPlot');
		if ($standardPlot.val().length == 0) {
			$standardPlot.parent().addClass('has-error');
		} else {
			$standardPlot.parent().removeClass('has-error');
		}
	}
	*/

	this.testWormsFound = function () {
		self.wormData.samplingStarted = true;
		var $wormsFound = $('#wormsFound');
		if ($wormsFound.val().length == 0) {
			$wormsFound.parent().addClass('has-error');
		} else {
			$wormsFound.parent().removeClass('has-error');
		}
		if ($wormsFound.val() == "Ei") {
			$("a[href='#detailsView']").attr('href', '#submitView');
		}
	}
	
	this.testMiddenCount = function () {
		// not required but must be a positive integer
		self.wormData.samplingStarted = true;
		var $middenCount = $('#middenCount');
		var count = parseInt($middenCount.val(),10);
		// console.log(count);
		if (count >= 0) {
			$middenCount.val(count);
			$middenCount.parent().removeClass('has-error');
		} else {
			$middenCount.val("");
			$middenCount.parent().addClass('has-error');
		}
	}
	
	this.isDate = function (txtDate) {
		// test txtDate for valid date format
		var currVal = txtDate;
		if(currVal == '')
			return false;

		var rxDatePattern = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/; //Declare Regex
		var dtArray = currVal.match(rxDatePattern); // is format OK?

		if (dtArray == null) 
			return false;

		//Checks for yyy/mm/dd format.
		dtMonth = dtArray[3];
		dtDay= dtArray[5];
		dtYear = dtArray[1];        

		if (dtMonth < 1 || dtMonth > 12) 
			return false;
		else if (dtDay < 1 || dtDay> 31) 
			return false;
		else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) 
			return false;
		else if (dtMonth == 2) 
		{
			var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
			if (dtDay> 29 || (dtDay ==29 && !isleap)) 
					return false;
		}
		return true;
	}
			
    this.initialize();

}