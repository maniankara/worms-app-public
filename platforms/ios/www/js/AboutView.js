var AboutView = function (wormData) {

	this.initialize = function () {
        // Define a div wrapper for the view (used to attach events)
        this.$el = $('<div class="templatehtml"/>');
        this.wormData = wormData[wormData.length - 1];
    };
    
    this.render = function() {
        this.$el.html(this.template());
        $('#version').text('version');
        return this;
	};
	
	this.showVersion = function() {
		$('#version').text('Version ' + this.wormData.appVersion);
	}
		
    this.initialize();

}