var HomeView = function (wormData) {

	this.initialize = function () {
        // Define a div wrapper for the view (used to attach events)
        this.$el = $('<div/>');
//         console.log('HomeView');
    };
        
    this.render = function() {
        this.$el.html(this.template());
        return this;
	};
	
		// resize the home content div to match the viewport			
	this.resizehomediv = function () {
		windowWidth = $(window).width();
		windowHeight = $(window).height();
// 		alert('height: ' + windowHeight +  'width: ' + windowWidth);
		$('.homeScreen').css({'height': windowHeight + 10});
		if (windowHeight > windowWidth) {
			$('#magImg').css({ 'margin-left': '0%'});
		} else {
			$('#magImg').css({ 'margin-left': '10%'});
		}
	};

    this.initialize();

}

